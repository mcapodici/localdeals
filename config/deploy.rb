require "rvm/capistrano"
require "bundler/capistrano"
require "whenever/capistrano"
require "dotenv/capistrano"

ssh_options[:forward_agent] = true
default_run_options[:pty] = true

set :rvm_ruby_string, :local              # use the same ruby as used locally for deployment
set :rvm_autolibs_flag, "read-only"       # more info: rvm help autolibs

before 'deploy:setup', 'rvm:install_rvm'  # install/update RVM
before 'deploy:setup', 'rvm:install_ruby' # install Ruby and create gemset

set :deploy_to, "/var/www/localdeals"
set :application, "localdeals"
set :repository,  "ssh://git@bitbucket.org/mcapodici/localdeals.git"
set :scm, :git
set :user, "deploy"
set :normalize_asset_timestamps, false

set :whenever_command, "bundle exec whenever"

after "deploy", "deploy:migrate"

# set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`
server "106.187.100.231", :app, :web, :db, :primary => true
#role :web, "your web-server here"                          # Your HTTP server, Apache/etc
#role :app, "your app-server here"                          # This may be the same as your `Web` server
#role :db,  "your primary db-server here", :primary => true # This is where Rails migrations will run
#role :db,  "your slave db-server here"#

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
 namespace :deploy do
   task :start do ; end
   task :stop do ; end
   task :restart, :roles => :app, :except => { :no_release => true } do
     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
   end
 end

set :linked_files, %w{.env}