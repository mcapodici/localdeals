Localdeals::Application.routes.draw do

  match 'poi' => 'home#poi', via:[:get]
  match 'admin' => 'admin#index', via:[:get]
  match 'admin/scrapecache' => 'admin#scrapecache', via:[:get]
  match 'admin/scrapecachecontents' => 'admin#scrapecache_contents', via:[:get]
  match 'admin/scrapecacheremove' => 'admin#scrapecache_remove', via:[:get]
  match 'admin/points' => 'admin#points', via:[:get]
  match 'admin/subscribers' => 'admin#subscribers', via:[:get]
  match 'deals' => 'home#deals', via:[:get]
  match 'finddeals' => 'home#find_deals', via:[:get]
  match 'gotodeal/:id' => 'home#go_to_deal', via:[:get]
  match 'subscribe' => 'subscribe#subscribe', via:[:get]
  match 'subscribe' => 'subscribe#subscribe_post', via:[:post], :as => :subscriber
  match 'subscribe_options' => 'subscribe#subscribe_options', via:[:get]
  match 'subscribe_options' => 'subscribe#subscribe_options_post', via:[:post, :put]


  #marketing routes
  match 'lanecove' => 'subscribe#squeeze', via:[:get], :lq => 'Lane Cove NSW, Australia', :location_text => 'Lane Cove'



  root :to => 'home#index'

end
