class AddKeyToSubscriber < ActiveRecord::Migration
  def change
    add_column :subscribers, :key, :string
  end
end
