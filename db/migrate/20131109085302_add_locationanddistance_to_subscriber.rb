class AddLocationanddistanceToSubscriber < ActiveRecord::Migration
  def change
    add_column :subscribers, :location, :string
    add_column :subscribers, :distance, :integer
  end
end
