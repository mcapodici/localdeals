class AddLastEmailTimeToSubscribers < ActiveRecord::Migration
  def change
    add_column :subscribers, :last_email_time, :datetime
  end
end
