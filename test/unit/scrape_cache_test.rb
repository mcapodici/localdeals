require 'test_helper'
require 'delorean'

class ScrapeCacheTest < Test::Unit::TestCase

  def teardown
    Delorean.back_to_the_present
  end

  def setup
    mongo = MongoConnect.new()
    fromdb = mongo.scrapecache.remove()
  end
  def test_storage
    assert_equal("456",ScrapeCache.with_web_cache("123", 1) { |url| "456" })
    assert_equal("456",ScrapeCache.with_web_cache("123", 1) { |url| "456-1" })
    assert_equal("457",ScrapeCache.with_web_cache("124", 1) { |url| "457" })
    assert_equal("457",ScrapeCache.with_web_cache("124", 1) { |url| "457-1" })
  end
  def test_expiry
    referenceTime = Time.now
    Delorean.time_travel_to referenceTime
    assert_equal("456",ScrapeCache.with_web_cache("123", 1) { |url| "456" })
    Delorean.time_travel_to referenceTime + 3600
    assert_equal("456",ScrapeCache.with_web_cache("123", 1) { |url| "456-1" })
    Delorean.time_travel_to referenceTime + 3700
    assert_equal("456-1",ScrapeCache.with_web_cache("123", 1) { |url| "456-1" })
  end
end
