require 'test_helper'
require 'delorean'

class TestAdd < Test::Unit::TestCase

  def teardown
    Delorean.back_to_the_present
  end

  def test_hash
    Delorean.time_travel_to Time.parse('2013-12-12T04:59:59Z')
    point = Point.new()
    point.id = "testid"
    point.title = "testtitle"
    point.detail = "testdetail"
    point.lat = 10.5
    point.lon = -30
    point.url = "testurl"
    point.provider = "god"
    point.city = "New York"

    expected_hash = {"id"=>"testid",
            "title"=>"testtitle",
            "detail"=>"testdetail",
            "location"=> { "type" => "Point", "coordinates" => [ -30, 10.5 ] },
            "url"=>"testurl",
            "expires_at"=>nil,
            "last_updated_time"=>Time.parse('2013-12-12T04:59:59Z'),
            "provider"=>"god",
            "city"=>"New York"
    }

    assert_equal expected_hash, point.to_db_hash
  end

  def test_equals
    Delorean.time_travel_to Time.parse('2013-12-12T04:59:59Z')
    point1 = Point.new()
    point1.id = "testid"
    point1.title = "testtitle"
    point1.detail = "testdetail"
    point1.lat = 10.5
    point1.lon = -30
    point1.url = "testurl"
    point2 = Point.new()
    point2.id = "testid"
    point2.title = "testtitle"
    point2.detail = "testdetail"
    point2.lat = 10.5
    point2.lon = -30.1
    point2.url = "testurl"
    point3 = Point.new()
    point3.id = "testid"
    point3.title = "testtitle"
    point3.detail = "testdetail"
    point3.lat = 10.5
    point3.lon = -30
    point3.url = "testurl"
    point4 = Point.new()
    point4.id = "testid"
    point4.title = "testtitle"
    point4.detail = "testdetail"
    point4.lat = 10.5
    point4.lon = -30
    point4.url = "testurl"
    point4.expires_at = Time.parse('2013-12-18T04:59:59Z')

    assert_equal point1, point3
    assert_not_equal point1, point2
    assert_not_equal point1, point4

  end
end
