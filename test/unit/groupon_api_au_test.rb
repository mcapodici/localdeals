require 'test_helper'
require 'delorean'

class GrouponApiAUTest < Test::Unit::TestCase

  def teardown
    Delorean.back_to_the_present
  end

  def test_with_file_1
    Delorean.time_travel_to Time.parse('2013-12-12T04:59:59Z')
    scrape = mock('scrape')
    scrape.expects(:get_url).with('http://dealhub.groupon.com.au/buckets/', 24).returns('<a href="file.json">')
    scrape.expects(:get_url).with('http://dealhub.groupon.com.au/buckets/file.json', 1).returns(File.read("#{Rails.root}/test/unit/groupon_api_au_test_1.json"))
    scrape.expects(:get_url).with('groupon_api_au_test_1.html', 1).returns(File.read("#{Rails.root}/test/unit/groupon_api_au_test_1.html"))
    Scrape.stubs(:instance).returns(scrape)
    api = GrouponApiAU.new
    expected_page_urls = ['http://dealhub.groupon.com.au/buckets/file.json']
    assert_equal(expected_page_urls, api.get_page_urls)

    actual_data_items = api.get_data_from_page(expected_page_urls[0]);
    actual_point = api.process_data_item(actual_data_items[0])
    expected_point = Point.new
    expected_point.detail = "$69 Original Keratin Treatment or $79 to Include Style Cut at Ahead of Hair, Bankstown (Up to $385 Value)"
    expected_point.id = "gau717504768"
    expected_point.lat = -33.914637
    expected_point.lon = 151.034145
    expected_point.title = "Ahead of Hair"
    expected_point.url = "groupon_api_au_test_1.html"
    expected_point.last_updated_time = Time.parse('2013-12-12T04:59:59Z')
    expected_point.expires_at = Time.parse('2013-12-16T07:35:27Z')
    expected_point.provider = 'gau'
    expected_point.city = 'Bankstown'
    assert_equal(expected_point, actual_point)
  end

  #def test_handy_runner
  #  ScrapeRunner.new.run('GrouponApiAU')
  #end
end
