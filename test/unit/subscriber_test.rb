require 'test_helper'

class SubscriberTest < ActiveSupport::TestCase

  def make_standard_subscriber
    subscriber = Subscriber.new
    subscriber.location = 'here'
    subscriber.distance = 1
    subscriber.name = 'me'
    subscriber.email = 'me@domain.com'
    subscriber
  end


  test 'key creation and uniqueness' do
    usedKeys = Set.new
    (1..10).each { |i|
      subscriber = make_standard_subscriber
      subscriber.email = "me#{i}@domain.com"
      assert_equal(nil, subscriber.key, 'key not set until save')
      assert(subscriber.save, 'successful save')
      assert_equal(36, subscriber.key.length, '36 character key generated')
      assert(!usedKeys.include?(subscriber.key), 'key is unique')
      usedKeys.add(subscriber.key)
      assert(usedKeys.include?(subscriber.key), 'anality: testing the test methodology :-)')
    }
  end

  test 'key not changed on edit' do
    subscriber = make_standard_subscriber
    assert(subscriber.save, 'successful save')
    assert_equal(36, subscriber.key.length, '36 character key generated')
    firstKey = subscriber.key
    subscriber.location = 'there'
    assert(subscriber.save, 'successful save')
    assert_equal(firstKey, subscriber.key, 'key only generated on first save')
  end

  test 'required fields' do
    assert_required(make_standard_subscriber, :location)
    assert_required(make_standard_subscriber, :distance)
    assert_required(make_standard_subscriber, :name)
    assert_required(make_standard_subscriber, :email)
  end

  test 'email formats' do
    subscriber = make_standard_subscriber
    subscriber.email = 'badaddress'
    assert !subscriber.valid?
    assert_equal ['is invalid'], subscriber.errors[:email]
    subscriber.email = 'good@email.address'
    assert subscriber.valid?
  end

  test 'email uniqueness' do
    make_standard_subscriber.save
    subscriber_with_same_email = make_standard_subscriber
    subscriber_with_same_email.location = 'another location'
    subscriber_with_same_email.distance = 99
    subscriber_with_same_email.name = 'another name'
    assert !subscriber_with_same_email.valid?
    assert_equal ['has already been taken'], subscriber_with_same_email.errors[:email]
  end
end
