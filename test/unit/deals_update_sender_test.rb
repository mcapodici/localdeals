require 'test_helper'

class DealsUpdateSenderTest < ActiveSupport::TestCase

  def setup
    clear_mongodb

    @referenceTime = Time.now
    Time.stubs(:now).returns(@referenceTime)
  end

  def test_happy_path

    subscriber = Subscriber.new
    subscriber.name = 'Henry'
    subscriber.email = 'h@e.com'
    subscriber.distance = 10
    subscriber.location = 'new york'
    subscriber.last_email_time = @referenceTime - 1.days
    subscriber.save

    point = Point.new()
    point.id = "testid"
    point.title = "testtitle"
    point.detail = "testdetail"
    point.lat = 10.5
    point.lon = -30
    point.url = "testurl"
    point.provider = "god"
    point.city = "New York"
    point.last_updated_time = @referenceTime - 12.hours
    point.expires_at = @referenceTime + 12.hours
    MongoConnect.new.points.insert(point.to_db_hash)

    point2 = Point.new()
    point2.id = "testid2"
    point2.title = "testtitle2"
    point2.detail = "testdetail2"
    point2.lat = 10.5001
    point2.lon = -30
    point2.url = "testurl5"
    point2.provider = "god"
    point2.city = "Manhatten"
    point2.last_updated_time = @referenceTime - 12.hours
    point2.expires_at = @referenceTime + 12.hours
    MongoConnect.new.points.insert(point2.to_db_hash)

    oldpoint = Point.new()
    oldpoint.id = "testid3"
    oldpoint.title = "testtitle3"
    oldpoint.detail = "testdetail3"
    oldpoint.lat = 10.5002
    oldpoint.lon = -30
    oldpoint.url = "testurl5"
    oldpoint.provider = "god"
    oldpoint.city = "Manhatten"
    oldpoint.last_updated_time = @referenceTime - 36.hours
    oldpoint.expires_at = @referenceTime + 12.hours
    MongoConnect.new.points.insert(oldpoint.to_db_hash)

    assert_equal(3,DealFinder.new.find_deals(10.5, -30).count,'Checking that the deals can be found')

    georesult = mock('Georesult')
    georesult.stubs(:latitude).returns(10.5)
    georesult.stubs(:longitude).returns(-30)
    georesult.stubs(:country_code).returns('us')
    Geocoder.stubs(:search).with('new york').returns([georesult])

    DealsUpdateSender.new.run

    assert_equal(@referenceTime.utc.change(:usec=>0), Subscriber.find_by_email('h@e.com').last_email_time.utc)

    assert !ActionMailer::Base.deliveries.empty?
    email = ActionMailer::Base.deliveries.last
    assert_equal ['deals@spoffa.com'], email.from
    assert_email_html_part_contains("a href=\"http://localhost:3000/subscribe_options?key=#{subscriber.key}\">", email)
    assert_email_html_part_contains('testtitle', email)
    assert_email_html_part_contains('testdetail', email)
    assert_email_html_part_contains('testtitle2', email)
    assert_email_html_part_contains('testdetail2', email)
    assert_email_plain_part_contains("http://localhost:3000/subscribe_options?key=#{subscriber.key}", email)
    assert_email_plain_part_contains('testtitle', email)
    assert_email_plain_part_contains('testdetail', email)
    assert_email_plain_part_contains('testtitle2', email)
    assert_email_plain_part_contains('testdetail2', email)
    assert_email_html_part_not_contains('testtitle3', email)
    assert_email_plain_part_not_contains('testtitle3', email)
  end


end