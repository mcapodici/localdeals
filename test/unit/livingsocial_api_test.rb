require 'test_helper'
require 'delorean'

class LivingSocialApiTest < Test::Unit::TestCase

  def teardown
      Delorean.back_to_the_present
  end

  def test_with_file_1
    Delorean.time_travel_to Time.parse('2013-12-12T04:59:59Z')
    scrape = MockScrape.new
    Scrape.stubs(:instance).returns(scrape)
    api = LivingSocialApi.new

    expected_page_urls = ['http://monocle.livingsocial.com/v2/deals?city=1&full=1&api-key=6AD78E52E910472F8412592FF1580511',
                          'http://monocle.livingsocial.com/v2/deals?city=2&full=1&api-key=6AD78E52E910472F8412592FF1580511']
    assert_equal(expected_page_urls, api.get_page_urls)

    actual_data_items = api.get_data_from_page(expected_page_urls[0]);
    assert_equal("https://www.livingsocial.com/deals/784166-eyelash-extensions-or-skin-services", actual_data_items[0]["url"])

    actual_point = api.process_data_item(actual_data_items[0])
    expected_point = Point.new
    expected_point.detail = "Eyelash Extensions, Makeup Application, or Facial at Eco-Friendly Salon"
    expected_point.id = "lsg784166"
    expected_point.lat = 42.3066575
    expected_point.lon = -87.9676795
    expected_point.title = "Art & Nature Eco-Salon"
    expected_point.url = "https://www.livingsocial.com/deals/784166-eyelash-extensions-or-skin-services"
    expected_point.expires_at = Time.parse('2013-10-23T09:59:00Z')
    expected_point.last_updated_time = Time.parse('2013-12-12T04:59:59Z')
    expected_point.provider = 'lsg'
    expected_point.city = 'Libertyville'
    assert_equal(expected_point, actual_point)
  end

  class MockScrape
    def get_url(url, l)
      case url
        when 'http://www.livingsocial.com/services/city/v2/cities'
          return File.read("#{Rails.root}/test/unit/livingsocial_api_test_1_cities.json")
        when 'http://monocle.livingsocial.com/v2/deals?city=1&full=1&api-key=6AD78E52E910472F8412592FF1580511'
          return File.read("#{Rails.root}/test/unit/livingsocial_api_test_1.json")

        when 'http://monocle.livingsocial.com/v2/deals?city=2&full=1&api-key=6AD78E52E910472F8412592FF1580511'
          return '{"deals": []}'

        else
          return nil
      end
    end
  end
end