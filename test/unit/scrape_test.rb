require 'test_helper'
require 'delorean'
require 'webmock/test_unit'

class TestAdd < Test::Unit::TestCase
  def teardown
    Delorean.back_to_the_present
  end
  def setup
    mongo = MongoConnect.new()
    fromdb = mongo.scrapecache.remove()
  end
  def test_storage
    scrape = Scrape.instance
    referenceTime = Time.now
    Delorean.time_travel_to referenceTime
    stub_request(:get, "http://www.test.com/").to_return(:body => "{hello:'world'}", :headers => {"Content-Type" => 'application/json'})
    result1 = scrape.get_url("http://www.test.com",1)
    stub_request(:get, "http://www.test.com/").to_return(:body => "{hello:'world2'}", :headers => {"Content-Type" => 'application/json'})
    result2 = scrape.get_url("http://www.test.com",1)
    assert_equal("{hello:'world'}", result2.to_s)
    Delorean.time_travel_to referenceTime + 3700
    result2 = scrape.get_url("http://www.test.com",1)
    assert_equal("{hello:'world2'}", result2.to_s)
  end
end


