require 'test_helper'
require 'delorean'

class GrouponApiUSTest < Test::Unit::TestCase

  def teardown
    Delorean.back_to_the_present
  end

  def test_with_file_1
    Delorean.time_travel_to Time.parse('2013-12-12T04:59:59Z')
    scrape = MockScrape.new
    scrape.data = File.read("#{Rails.root}/test/unit/groupon_api_us_test_1.json")
    Scrape.stubs(:instance).returns(scrape)
    api = GrouponApiUS.new

    expected_page_urls = ['http://api.groupon.com/v2/deals?client_id=092881d865d3ac13e90caaa6a67a48c922f54c0f&division_id=div1']
    assert_equal(expected_page_urls, api.get_page_urls)

    actual_data_items = api.get_data_from_page(expected_page_urls[0]);
    assert_equal("http://www.groupon.com/deals/ga-go-today-2-italy", actual_data_items[0]["dealUrl"])

    actual_point = api.process_data_item(actual_data_items[0])
    expected_point = Point.new
    expected_point.detail = "10-Day Rome, Florence, and Tuscan Villa Vacation with Airfare from go-today"
    expected_point.id = "gusga-go-today-2-italy"
    expected_point.lat = 41.90925
    expected_point.lon = 12.4984661
    expected_point.title = "Three-City Italian Vacation with Airfare"
    expected_point.url = "http://www.groupon.com/deals/ga-go-today-2-italy"
    expected_point.expires_at = Time.parse('2013-12-18T04:59:59Z')
    expected_point.last_updated_time = Time.parse('2013-12-12T04:59:59Z')
    expected_point.provider = 'gus'
    expected_point.city = 'Rome'
    assert_equal(expected_point, actual_point)
  end

  class MockScrape
    attr_accessor :data

    def get_url(url, l)
      return '{"divisions":[{"id":"div1"}]}' if url.include? 'divisions?'
      return @data if url.include? 'div1'
      return '{"deals":[]}'
    end
  end
end
