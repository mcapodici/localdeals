require 'test_helper'
require 'delorean'

# You can run the scrape runner for real in dev environment using:
# script/rails runner -e development 'ScrapeRunner.new.run'

class ScrapeRunnerTest < ActiveSupport::TestCase
  def setup
    clear_mongodb

    @referenceTime = Time.now
    Delorean.time_travel_to @referenceTime
  end
  def example_point
    point = Point.new
    point.id = "testid"
    point.title = "testtitle"
    point.detail = "testdetail"
    point.lat = 10.5
    point.lon = -30
    point.url = "testurl"
    point.expires_at = @referenceTime + 5.days
    point.last_updated_time = @referenceTime
    point
  end
  def test_clears_expired
    mongo = MongoConnect.new
    expired_point = example_point
    expired_point.lat += 1
    expired_point.id = 'removedid'
    expired_point.expires_at = @referenceTime - 1.hour
    mongo.points.insert(expired_point.to_db_hash, {:w => 1})
    runner = ScrapeRunner.new
    runner.expects(:get_scrapers).returns([create_happy_path_scraper])
    runner.run
    assert_equal(1,mongo.points.count())
    assert_equal('testid', mongo.points.find().first()['id'])
  end
  def create_happy_path_scraper
    scraper = mock('scraper')
    scraper.expects(:get_page_urls).returns(['http://a.b/1'])
    scraper.expects(:get_data_from_page).with('http://a.b/1').returns(['{page:data}'])
    scraper.expects(:process_data_item).with('{page:data}').returns(example_point)
    scraper
  end
  def test_happy_path
    runner = ScrapeRunner.new
    runner.expects(:get_scrapers).returns([create_happy_path_scraper])
    runner.run
    assert_log_contains("Running scraper Mocha::Mock >>>
Pages to scrape >>
http://a.b/1
<< Pages to scrape
Scraping url http://a.b/1
<<< Scrape completed. Updated 0 existing points, inserted 1 new points.")
    assert_equal(1,MongoConnect.new.points.count())
  end
  def test_with_duplicate_in_scrape
    scraper = mock('scraper')
    scraper.expects(:get_page_urls).returns(['http://a.b/1','http://a.b/2','http://a.b/3'])
    scraper.expects(:get_data_from_page).with('http://a.b/1').returns(['{page:data}'])
    scraper.expects(:get_data_from_page).with('http://a.b/2').returns(['{page:data2}'])
    scraper.expects(:get_data_from_page).with('http://a.b/3').returns(['{page:data3}'])
    scraper.expects(:process_data_item).with('{page:data}').returns(example_point)

    dupe_point = example_point()
    dupe_point.expires_at += 24 * 3600
    dupe_point.id += "2"
    scraper.expects(:process_data_item).with('{page:data2}').returns(dupe_point)

    nodupe_point = example_point()
    nodupe_point.lat += 1
    nodupe_point.id += "3"
    scraper.expects(:process_data_item).with('{page:data3}').returns(nodupe_point)

    runner = ScrapeRunner.new
    runner.expects(:get_scrapers).returns([scraper])
    runner.run
    assert_log_contains("Running scraper Mocha::Mock >>>
Pages to scrape >>
http://a.b/1
http://a.b/2
http://a.b/3
<< Pages to scrape
Scraping url http://a.b/1
Scraping url http://a.b/2
Scraping url http://a.b/3
<<< Scrape completed. Updated 1 existing points, inserted 2 new points.")
  end
  def test_with_duplicate_scrape_vs_existing
    #todo can we get this bloody mongo to always use safe inserts so we dont need to specify it per query??
    MongoConnect.new.points.insert(example_point.to_db_hash)
    scraper = mock('scraper')
    scraper.expects(:get_page_urls).returns(['http://a.b/1','http://a.b/2'])
    scraper.expects(:get_data_from_page).with('http://a.b/1').returns(['{page:data}'])
    scraper.expects(:get_data_from_page).with('http://a.b/2').returns(['{page:data2}'])

    dupe_point = example_point()
    dupe_point.expires_at += 24 * 3600
    dupe_point.id += "2"
    scraper.expects(:process_data_item).with('{page:data}').returns(dupe_point)

    nodupe_point = example_point()
    nodupe_point.lat += 1
    nodupe_point.id += "3"
    scraper.expects(:process_data_item).with('{page:data2}').returns(nodupe_point)

    runner = ScrapeRunner.new
    runner.expects(:get_scrapers).returns([scraper])
    runner.run
    assert_log_contains("Running scraper Mocha::Mock >>>
Pages to scrape >>
http://a.b/1
http://a.b/2
<< Pages to scrape
Scraping url http://a.b/1
Scraping url http://a.b/2
<<< Scrape completed. Updated 1 existing points, inserted 1 new points.
")
  end

  def test_general_error
    runner = ScrapeRunner.new
    runner.expects(:get_scrapers).raises(StandardError)
    runner.run
    assert_log_contains("Exception occurred during general scrape runner process.")
  end
  def test_get_page_urls_error
    scraper = mock('scraper')
    scraper.expects(:get_page_urls).raises(StandardError)
    runner = ScrapeRunner.new
    runner.expects(:get_scrapers).returns([scraper])
    runner.run
    assert_log_contains('Running scraper Mocha::Mock >>>
Exception occurred during Mocha::Mock scraper (get_pages).
Error Message: StandardError
Error Backtrace:')
  end
  def test_get_data_from_page_error
    scraper = mock('scraper')
    scraper.expects(:get_page_urls).returns(['badpage','goodpage'])
    scraper.expects(:get_data_from_page).with('badpage').raises(StandardError)
    scraper.expects(:get_data_from_page).with('goodpage').returns(['{data:good}'])
    scraper.expects(:process_data_item).with('{data:good}').returns(example_point)
    runner = ScrapeRunner.new
    runner.expects(:get_scrapers).returns([scraper])
    runner.run
    assert_log_contains("Running scraper Mocha::Mock >>>
Pages to scrape >>
badpage
goodpage
<< Pages to scrape
Scraping url badpage
Exception occurred during processing page badpage.
Error Message: StandardError
Error Backtrace:")
  end
  def test_data_item_error
    sequence = sequence('sequence')
    scraper = mock('scraper')
    scraper.expects(:get_page_urls).returns(['badpage','goodpage'])
    scraper.expects(:get_data_from_page).with('badpage').returns(['{data:bad}'])
    scraper.expects(:get_data_from_page).with('goodpage').returns(['{data:good}'])
    scraper.expects(:process_data_item).with('{data:bad}').raises(StandardError)
    scraper.expects(:process_data_item).with('{data:good}').returns(example_point)
    runner = ScrapeRunner.new
    runner.expects(:get_scrapers).returns([scraper])
    runner.run
    assert_log_contains("Running scraper Mocha::Mock >>>
Pages to scrape >>
badpage
goodpage
<< Pages to scrape
Scraping url badpage
Exception occurred during processing point. Dump of item then point follows.
Error Message: StandardError
Error Backtrace:")
  end
end
