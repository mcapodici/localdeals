ENV["RAILS_ENV"] = "test"
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'test/unit'
require 'mocha/mock'


class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.(yml|csv) for all tests in alphabetical order.
  #
  # Note: You'll currently still have to declare fixtures explicitly in integration tests
  # -- they do not yet inherit this setting
  fixtures :all
  setup :log_setup

  # Add more helper methods to be used by all tests here...
  def clear_mongodb
    m = MongoConnect.new
    m.client.drop_database(m.dbname)
  end

  def assert_required(obj, field)
    obj[field] = ''
    assert !obj.valid?
    assert_equal(["can't be blank"], obj.errors[field])
  end

  def assert_email_multi_part(fixture_name, email)
    assert_email_html_part(read_fixture(fixture_name + '.html').join, email)
    assert_email_plain_part(read_fixture(fixture_name + '.text').join, email)
  end

  def assert_email_html_part(expected, email)
    assert_equal expected, email_part(email, /html/)
  end

  def assert_email_plain_part(expected, email)
    assert_equal expected,  email_part(email, /plain/)
  end

  def assert_email_html_part_contains(expected, email)
    assert_contains expected, email_part(email, /html/)
  end

  def assert_email_plain_part_contains(expected, email)
    assert_contains expected,  email_part(email, /plain/)
  end

  def assert_email_html_part_not_contains(expected, email)
    assert_not_contains expected, email_part(email, /html/)
  end

  def assert_email_plain_part_not_contains(expected, email)
    assert_not_contains expected,  email_part(email, /plain/)
  end

  def log_setup
    log_file_path = ActiveSupport::TestCase.test_log_path
    File.truncate(log_file_path, 0) if File.exist?(log_file_path)
    LoggerHelper.stubs(:log_file_path).returns(log_file_path)
  end

  def log_teardown
    LoggerHelper.unstub(:log_file_path)
  end

  def self.test_log_path
    "#{Rails.root}/log/test1.log"
  end

  def assert_not_contains(expected_substring, string, *args)
    assert !string.include?(expected_substring), *args
  end

  def assert_contains(expected_substring, string, *args)
    if (!string.include?(expected_substring))
      puts Differ.diff_by_char(string, expected_substring)
    end

    assert string.include?(expected_substring), *args
  end

  def assert_log_contains(string)
    log_text = ''
    log_text = File.read(ActiveSupport::TestCase.test_log_path) if File.exist?(ActiveSupport::TestCase.test_log_path)

    assert_contains(string, log_text)
  end

  private

  def email_part(email, rx)
    email.body.parts.find { |p| p.content_type.match rx }.body.raw_source
  end
end

# To make debugging spec test with stubbed :id objects in debugger possible.
Mocha::Mock.class_eval <<-MOCHA
  # alias_method :mock_respond_to, :respond_to?
  # def respond_to?(symbol, include_private = false)
  #   mock_respond_to(symbol, include_private) || symbol == :object_id
  # end
  #
  # the above approach would cause broken debugger loop due to error "Exception in DebugThread loop: unexpected invocation: #<Mock:image1>.mock_respond_to(:object_id, false)"
  #
  # so we have to resort to copying codes
  def respond_to?(symbol, include_private = false)
    return true if symbol == :object_id
    if @responder then
      if @responder.method(:respond_to?).arity > 1
        @responder.respond_to?(symbol, include_private)
      else
        @responder.respond_to?(symbol)
      end
    else
      @everything_stubbed || @expectations.matches_method?(symbol)
    end
  end
MOCHA

require 'mocha/setup'
