require 'test_helper'

class AdminControllerTest < ActionController::TestCase
  test "should require password to get index" do
    get :index
    assert_response 401
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials('admin', 'password')
    get :index
    assert_response :success
    ENV['ADMIN_PASSWORD'] = nil
    get :index
    assert_response 401
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials('admin', 'password')
    get :index
    assert_response 401
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials('admin', '')
    get :index
    assert_response 401
  end

end