require 'test_helper'

class DealsUpdateMailerTest < ActionMailer::TestCase

  test 'welcome_email content' do
    subscriber = Subscriber.new
    subscriber.location = 'new orleans'
    subscriber.distance = 10
    subscriber.name = 'Jonathan'
    subscriber.email = 'j@e.com'
    subscriber.key = '1111-2222-3333-4444'
    email = DealsUpdateMailer.welcome_email(subscriber).deliver
    assert !ActionMailer::Base.deliveries.empty?
    assert_equal ['deals@spoffa.com'], email.from
    assert_equal ['j@e.com'], email.to
    assert_equal 'Thank you for subscribing to Spoffa', email.subject
    assert_email_multi_part 'welcome_email', email
  end

end

