require 'test_helper'

class SubscribeControllerTest < ActionController::TestCase

  test "should get subscribe" do
    get :subscribe
    assert_response :success
  end

  test "squeeze pages work" do
    assert_recognizes({:controller => 'subscribe', :action => 'squeeze', :lq => 'Lane Cove NSW, Australia', :location_text => 'Lane Cove'},
                      {:path => '/lanecove', :method => :get})
    get :squeeze,  :lq => 'Lane Cove NSW, Australia', :location_text => 'Lane Cove'
    assert_response :success
  end

  test "submit subscribe causes email to be sent" do
    response = post :subscribe_post,
                    {
                        :subscriber => {
                            :location => 'new orleans',
                            :distance => 10,
                            :email => 'j@e.com',
                            :name => 'Jonathan'
                        }
                    }
    assert !ActionMailer::Base.deliveries.empty?
    email = ActionMailer::Base.deliveries.last
    assert_equal ['deals@spoffa.com'], email.from
    assert_equal ['j@e.com'], email.to
    assert_equal 'Thank you for subscribing to Spoffa', email.subject

    subscriber = Subscriber.find_by_email('j@e.com')

    assert_email_html_part_contains( 'Welcome to Spoffa alerts, Jonathan' , email)
    assert_email_html_part_contains( 'Your chosen location is new orleans and we will send you deals
  within 10 kms.' , email)
    assert_email_html_part_contains( "http://localhost:3000/subscribe_options?key=#{subscriber.key}", email)
  end
end