require 'test_helper'

class HomeControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "go to deal with valid" do
    deal_finder = DealFinder.new
    DealFinder.expects(:new).returns(deal_finder)
    deal_finder.expects(:get_url).with('id001').returns('http://www.link.com')
    result = get(:go_to_deal, {:id => 'id001'})
    assert_equal 302, result.status
    assert_equal 'http://www.link.com', result.header['Location']
  end

  test "go to deal with invalid" do
    deal_finder = DealFinder.new
    DealFinder.expects(:new).returns(deal_finder)
    deal_finder.expects(:get_url).with('id001').returns(nil)
    result = get(:go_to_deal, {:id => 'id001'})
    assert_equal 302, result.status
    assert_equal 'http://test.host/', result.header['Location']
  end

  test "test finddeals with no query" do
    Geocoder.expects(:search).with(nil).returns(nil)
    result = get(:find_deals)
    assert_equal([], assigns(:points))
    assert_equal(true, assigns(:not_recognised))
  end

  test "test finddeals with query that cant be located" do
    Geocoder.expects(:search).with('mars').returns(nil)
    result = get(:find_deals, {:lq => 'mars'})
    assert_equal([], assigns(:points))
    assert_equal(true, assigns(:not_recognised))
  end

  test "test finddeals with query that returns no results" do
    georesult = mock('georesult')
    georesult.expects(:latitude).returns(10)
    georesult.expects(:longitude).returns(10)
    georesult.expects(:address).returns('alaska')
    georesult.expects(:country_code).returns('us')
    Geocoder.expects(:search).with('alaska').returns([georesult])

    deal_finder = DealFinder.new
    deal_finder.stubs(:find_deals).with(10, 10, 1).returns([])
    DealFinder.expects(:new).returns(deal_finder)

    result = get(:find_deals, {:lq => 'alaska'})
    assert_equal([], assigns(:points))
    assert_equal(false, assigns(:not_recognised))
  end

  test "test finddeals with query that returns results" do

    point = PointForView.new
    point.id = "testid"
    point.title = "testtitle"
    point.detail = "testdetail"
    point.lat = 10.5
    point.lon = -30
    point.url = "testurl"
    point.expires_at = Time.now
    point.last_updated_time = Time.now
    point.lat_searched = 10
    point.lon_searched = 10

    georesult = mock('georesult')
    georesult.expects(:latitude).returns(10)
    georesult.expects(:longitude).returns(10)
    georesult.expects(:address).returns('alaska')
    georesult.expects(:country_code).returns('us')
    Geocoder.expects(:search).with('alaska').returns([georesult])

    deal_finder = DealFinder.new
    deal_finder.stubs(:find_deals).with(10, 10, 1).returns([point])
    DealFinder.expects(:new).returns(deal_finder)
    result = get(:find_deals, {:lq => 'alaska'})
    assert_equal([point], assigns(:points))
    assert_equal(false, assigns(:not_recognised))
  end

end