class DealsUpdateMailer < ActionMailer::Base
  default from: "deals@spoffa.com"

  def welcome_email(subscriber)
    @subscriber = subscriber
    mail(to: @subscriber.email, subject: 'Thank you for subscribing to Spoffa')
  end

  def update_email(subscriber, points, lat, lon)
    @subscriber = subscriber
    @points = points
    @lat = lat
    @lon = lon
    mail(to: @subscriber.email, subject: "Here are today's Spoffa updates")
  end
end
