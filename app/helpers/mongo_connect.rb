require 'mongo'
include Mongo

class MongoConnect

  def client
    return @client
  end

  def db
    return @db
  end

  def points
    return @db["points"]
  end

  def scrapecache
    return @db["scrapecache"]
  end

  def dbname
    "deals" + Rails.env
  end

  def initialize
    @client = MongoClient.new("localhost", {:w=>1, :j=>true})
    #todo: move this into config
    @db = @client.db(dbname)
    points.ensure_index({ "location" => "2dsphere"} )
    points.ensure_index( { "id" => 1 }, { "unique" => true } )
    scrapecache.ensure_index( {"key" => 1})
  end

end
