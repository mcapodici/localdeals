class ScheduledRun

  def run

    begin
      ScrapeRunner.new.run
    rescue StandardError => err
      LoggerHelper.get_singleton('scheduled-run').error(err,"Exception occurred during general scrape runner process.")
    end

    begin
      DealsUpdateSender.new.run
    rescue StandardError => err
      LoggerHelper.get_singleton('scheduled-run').error(err,"Exception occurred during general sending of new deals.")
    end

  end

end