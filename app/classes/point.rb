class Point
  attr_accessor :id
  attr_accessor :title
  attr_accessor :detail
  attr_accessor :lat
  attr_accessor :lon
  attr_accessor :url
  attr_accessor :expires_at
  attr_accessor :last_updated_time     # we are actually now using this to mean first_inserted_time so need to update name at some point
  attr_accessor :provider
  attr_accessor :city

  def initialize
    @last_updated_time = Time.now.utc.change(:usec => 0)
  end

  def self.apply_db_hash(point, hash)
    point.id = hash['id']
    point.title = hash['title']
    point.detail = hash['detail']
    point.lon = hash['location']['coordinates'][0]
    point.lat = hash['location']['coordinates'][1]
    point.url = hash['url']
    point.expires_at = hash['expires_at']
    point.last_updated_time = hash['last_updated_time']
    point.provider = hash['provider']
    point.city = hash['city']
    point
  end

  def to_db_hash
    hash =
        {
            'id' => @id,
            'title' => @title,
            'detail' => @detail,
            'location' => { 'type' => 'Point', 'coordinates' => [@lon, @lat] },
            'url' => @url,
            'expires_at' => @expires_at,
            'last_updated_time' => @last_updated_time,
            'provider' => @provider,
            'city' => @city
        }
    hash
  end

  def ==(p)
    (self.instance_variables + p.instance_variables).uniq.each do |var|
      return false if self.instance_variable_get(var).inspect != p.instance_variable_get(var).inspect
    end

    return true
  end

end
