class ScrapeCache
  # Attempts to get cached result for key, but if not available will run
  # the block of code and return that (and cache it in the process)
  #
  # * *Args*    :
  #   - +key+ -> the key for looking up in the cache
  #   - +keepfor+ -> number of hours to keep in the cache
  # * *Returns* :
  #   - the contents of the cache for the key, or the result of calling the block if no cache available
  def self.with_web_cache(key, keepfor)

    mongo = MongoConnect.new()

    mongo.scrapecache.remove({:key => key, :expiry => {"$lt" => Time.now}})
    cache_result = mongo.scrapecache.find({:key => key}).first()

    if cache_result
      return cache_result["data"]
    else
      result = yield(key)
      mongo.scrapecache.insert({:key=>key, :expiry => Time.now + keepfor * 3600, :data =>result})
      result
    end
  end

end