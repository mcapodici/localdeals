class LoggerHelper

  @@singleton_loggers = {}

	def initialize(logfile)
		logfile = logfile || 'default'
		@logger = Logger.new(LoggerHelper.log_file_path(logfile))
	end

	def error(err, message)
		@logger.error("#{message}\nError Message: #{err.message}\nError Backtrace: #{err.backtrace.join("\n")}")
	end
	
	def debug(obj)
		@logger.debug(obj)
	end
	
	def info(message)
		@logger.info(message)
  end

  def self.get_singleton(logfile)
    @@singleton_loggers[logfile] ||= LoggerHelper.new(logfile)
  end

  def self.scrape_logger(logfileid)
    get_singleton("scrape-#{logfileid}-T#{Time.now.strftime('%H%M')}")
  end

  def self.log_file_path (logfile)
    "#{Rails.root}/log/#{logfile}-#{ Time.now.strftime('%Y-%m-%d')}.log"
  end

end