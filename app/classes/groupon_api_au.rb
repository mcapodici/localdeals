require 'mechanize'
require 'json'

class GrouponApiAU

  GROUPON_BUCKETS_URI = 'http://dealhub.groupon.com.au/buckets/'

  def get_page_urls
    bucketsHtml = Scrape.instance.get_url(GROUPON_BUCKETS_URI, 24)
    links = Nokogiri::HTML(bucketsHtml).css('a').to_a
    links.keep_if {|l| l['href'][-5,5] == '.json' }
    links.map{|l| URI::join(GROUPON_BUCKETS_URI,l['href']).to_s}
  end

  def get_data_from_page(url)
    JSON.load(Scrape.instance.get_url(url, 1)).to_a
  end

  def process_data_item(item)
    begin
      dealPage = Scrape.instance.get_url(item['dealDetailsUrl'], 1)
      timeleftSecs = Nokogiri::HTML(dealPage).css('.jcurrentTimeLeft')[0]['value'].to_i / 1000
      expires_at = Time.now.utc.change(:usec => 0) + timeleftSecs
    rescue StandardError
      #LoggerHelper.scrape_logger.info("Error trying to scrape time left for #{item['dealDetailsUrl']}, defaulting to now + 24hr")
      expires_at = Time.now.utc.change(:usec => 0) + 24 * 60 * 60
    end

    point = Point.new()
    point.provider = 'gau'
    point.id = 'gau' + item['id'] #g for groupon, au for australia
    point.title = item['merchant']
    point.detail = item['title']
    point.lat = item['lat'].to_f
    point.lon = item['lon'].to_f
    point.url = item['dealDetailsUrl']
    point.expires_at = expires_at
    point.city = item['city'].split.map(&:capitalize).join(' ')
    point
  end

end