class PointForView < Point
  attr_accessor :lat_searched
  attr_accessor :lon_searched

  def distance_miles
    Geocoder::Calculations.distance_between([@lat, @lon], [@lat_searched, @lon_searched])
  end

  def distance_kms
    distance_miles * 1.609344
  end

  def distance_metric_display
    dkms = distance_kms
    if (dkms < 1) then
      ('%.0f' % (dkms * 1000).round(-1)) + 'm'
    else
      ('%.1f' % dkms) + 'kms'
    end
  end

end