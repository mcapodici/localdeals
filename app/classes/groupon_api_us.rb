require 'mechanize'
require 'json'

class GrouponApiUS

  API_DIVISIONS_URL = 'http://api.groupon.com/v2/divisions?client_id=092881d865d3ac13e90caaa6a67a48c922f54c0f'
  API_ITEMS_URL = 'http://api.groupon.com/v2/deals?client_id=092881d865d3ac13e90caaa6a67a48c922f54c0f&division_id={division}'

  def get_page_urls
    divisions_string = Scrape.instance.get_url(API_DIVISIONS_URL, 24)
    divisions_json = JSON.load divisions_string
    divisions_json['divisions'].map{|d| API_ITEMS_URL.sub('{division}', d['id'])}.to_a
  end

  def get_data_from_page(url)
    JSON.load(Scrape.instance.get_url(url, 1))['deals'].to_a
  end

  def process_data_item(item)
    if item['options'] &&
        item['options'].count > 0 &&
        item['options'][0]['redemptionLocations'] &&
        item['options'][0]['redemptionLocations'].count > 0
      redemptionLocation1 = item['options'][0]['redemptionLocations'][0]
      point = Point.new()
      point.provider = 'gus'
      point.id = 'gus' + item['id']
      point.title = item['merchant']['name']
      point.detail = item['title']
      point.lat = redemptionLocation1['lat']
      point.lon = redemptionLocation1['lng']
      point.city = redemptionLocation1['city'].split.map(&:capitalize).join(' ')
      point.url = item['dealUrl']
      point.expires_at = Time.parse(item['options'][0]['expiresAt'])
      point
    else
      'skipped processing item ' + item['id'] + ' due to no redemptionLocations'
    end
  end

end
