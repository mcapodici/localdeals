require 'mechanize'
require 'json'

class LivingSocialApi

  API_CITIES_URL = 'http://www.livingsocial.com/services/city/v2/cities'
  API_ITEMS_URL = 'http://monocle.livingsocial.com/v2/deals?city={city}&full=1&api-key=6AD78E52E910472F8412592FF1580511'

  def get_page_urls
    cities_string = Scrape.instance.get_url(API_CITIES_URL, 24)
    cities_json = JSON.load cities_string
    cities_json.map{|c| API_ITEMS_URL.sub('{city}', c['id'].to_s)}.to_a
  end

  def get_data_from_page(url)
    JSON.load(Scrape.instance.get_url(url, 1))['deals'].to_a
  end

  def process_data_item(item)
    if item['locations'] &&
        item['locations'].count > 0 &&
        item['locations'][0] &&
        item['locations'][0]['latlng']

      point = Point.new()
      point.provider = 'lsg'
      point.id = 'lsg' + item['id'].to_s
      point.title = item['merchant_name']
      point.detail = item['long_title']
      point.lat = item['locations'][0]['latlng'][0].to_f
      point.lon = item['locations'][0]['latlng'][1].to_f
      point.city = item['locations'][0]['city']
      point.url = item['url']
      point.expires_at = Time.parse(item['offer_ends_at'])
      point
    else
      'skipped processing item ' + item['id'].to_s + ' due to no locations'
    end
  end

end
