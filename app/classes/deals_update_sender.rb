class DealsUpdateSender
  def run
    # need to find deals that are within the distance but
    # have been added since the last time this subscriber has
    # received an email

    # we need to send emails at most once per day later this
    # can be configured
    @logger = LoggerHelper.get_singleton('deals-update-sender')
    Subscriber.all().each { |s| send_deals_for_subscriber(s)}
  end

  def send_deals_for_subscriber(subscriber)

    begin
      #only send emails if they have not received one in the last day
      return if subscriber.last_email_time && subscriber.last_email_time > Time.now.utc - 1.day

      finder = DealFinder.new
      georesult = finder.geo_search(subscriber.location)

      #don't continue unless we have lat/lon
      return if !georesult

      finder = DealFinder.new
      finder.max_radius_kms = [subscriber.distance, 100].min
      finder.min_updated_time = subscriber.last_email_time || (Time.now - 1.day).utc
      points = finder.find_deals(georesult.latitude, georesult.longitude).to_a

      DealsUpdateMailer.update_email(subscriber, points, georesult.latitude, georesult.longitude).deliver
      @logger.info("Sent update email to: #{subscriber.email}")

      subscriber.last_email_time = Time.now.utc
      subscriber.save!
    rescue StandardError => err
      @logger.error(err,"Exception occurred during sending email to subscriber: #{subscriber.email}")
    end

  end

end