class ScrapeRunner

  def get_scrapers
    [GrouponApiAU.new,GrouponApiUS.new,LivingSocialApi.new]
  end

  def run(specific=nil)
    begin
      #todo what if we cant connect?
      mongo = MongoConnect.new()
      points_collection = mongo.points
      points_collection.ensure_index({ "location" => "2dsphere"} )
      points_collection.ensure_index( { "id" => 1 }, { "unique" => true } )

      get_scrapers.each { |scraper|

        next if specific && scraper.class.to_s != specific

        logger = LoggerHelper.scrape_logger(scraper.class)
        logger.info "Running scraper #{scraper.class} >>>"

        update_count = 0
        insert_count = 0

        urls = nil

        begin
          urls = scraper.get_page_urls
          logger.info("Pages to scrape >>")
          logger.info(urls.join("\n"))
          logger.info("<< Pages to scrape")
        rescue StandardError => err
          logger.error(err,"Exception occurred during #{scraper.class} scraper (get_pages).")
        end

        begin
          urls.each { |url|
            begin
              logger.info("Scraping url #{url}")
              page_data = scraper.get_data_from_page(url)
              point_data = page_data.map{ |item|
                begin
                  scraper.process_data_item(item)
                rescue StandardError => err
                  logger.error(err,"Exception occurred during processing point. Dump of item then point follows.")
                  logger.debug(item)
                  nil
                end
              }
              points = point_data.find_all{|p| p.is_a?(Point)}.group_by{|p|[p.title, p.lat, p.lon]}
              messages = point_data.find_all{|p| p && !p.is_a?(Point)}

              points.each { |k, points|
                begin
                  point = points.max_by{|p|p.expires_at }

                  # find any existing matching results
                  existing_similar_results = points_collection
                      .find({"title" => point.title, "location" => { "type" => "Point", "coordinates" => [point.lon, point.lat] }})
                      .sort({:last_updated_time=>1}).to_a

                  # remove any further duplicates there should just be one on the key
                  if !existing_similar_results.empty?
                    existing_similar_results[1..-1].each { |result|
                      points_collection.remove('_id' => result['_id'])
                    }
                  end

                  begin
                    if existing_similar_results.empty?
                      points_collection.insert(point.to_db_hash)
                      insert_count += 1
                    else
                      point.last_updated_time = existing_similar_results[0]["last_updated_time"]
                      points_collection.update({'_id' => existing_similar_results[0]['_id']}, point.to_db_hash)
                      update_count += 1
                    end
                  rescue Mongo::OperationFailure => e
                    if e.message =~ /^11000/
                      logger.error(e,'Duplicate key error')
                      logger.debug(point)
                    else
                      raise e
                    end
                  end

                rescue StandardError => err
                  logger.error(err,"Exception occurred during storing point. Dump of item then point follows.")
                  logger.debug(point)
                end
              }
              messages.each {|m|
                logger.info(m.to_s)
              }
            rescue StandardError => err
              logger.error(err,"Exception occurred during processing page #{url}.")
            end
          }
        end if urls

        logger.info("<<< Scrape completed. Updated #{update_count} existing points, inserted #{insert_count} new points.")
        remove_result = points_collection.remove("expires_at" => {"$lt" => Time.now.utc.change(:usec => 0) })
        logger.info("Removed #{remove_result['n']} expired points.")
      }
    rescue StandardError => err
      LoggerHelper.get_singleton('scrape-general').error(err,"Exception occurred during general scrape runner process.")
    ensure
      mongo.client.close() if mongo and mongo.client
    end
  end

end
