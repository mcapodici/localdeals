class DealFinder

  attr_accessor :results_per_page, :max_radius_kms, :min_updated_time

  def initialize
    @results_per_page = 25
    @max_radius_kms = 100
    @min_updated_time = nil
  end

  def find_deals(lat, lon, pageNumber = 1)
    mongo = MongoConnect.new()

    query = {
        "location" => {
            "$near" => {
                "$geometry" => {
                    "type" => "Point",
                    "coordinates" => [lon, lat]
                },
                "$maxDistance" => @max_radius_kms * 1000
            }
        },
        "expires_at" => {"$gt" => Time.now.utc.change(:usec => 0)}
    }

    query["last_updated_time"] = {"$gt" => @min_updated_time.utc} if @min_updated_time

    pointHashes = mongo.points.find(query, {:fields => {"_id" => 0}}).skip(@results_per_page * (pageNumber-1)).limit(@results_per_page)
    pointHashes.map { |hash|
      point = PointForView.apply_db_hash(PointForView.new, hash)
      point.lat_searched = lat
      point.lon_searched = lon
      point
    }
  end

  def get_url(id)
    mongo = MongoConnect.new()
    point = mongo.points.find_one(
        {"id" => id},
        {:fields => {"_id" => 0}})
    if point then
      point["url"]
    else
      nil
    end
  end

  def geo_search(query)
    allgeoresults = Geocoder.search(query) #, :region => 'au')
    allgeoresults.select { |res| res.country_code == "AU" or true }.first if allgeoresults
  end

end
