class Scrape
	
	include Singleton
	
	# Gets the contents of the url using Mechanize. Caches content
	#
	# * *Args*    :
	#   - +url+ -> url to scrape
	#   - +keepfor+ -> number of hours to keep in the cache
	# * *Returns* :
	#   - the results of @agent.get
	def get_url(url, keepfor)
		ScrapeCache.with_web_cache(url, keepfor) { |urltext| 
				a = Mechanize.new { |agent|
	 				agent.user_agent_alias = 'Mac Safari'
				}
				a.get(urltext).body
#				u = URI.parse(urltext)
#				Net::HTTP.get(u, {'User-Agent' => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:24.0) Gecko/20100101 Firefox/24.0'}) 
		}
	end 
end