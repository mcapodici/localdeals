require 'uuidtools'
require 'base64'

class Subscriber < ActiveRecord::Base
  attr_accessible :email, :name, :location, :distance

  validates_presence_of :email
  validates_presence_of :name
  validates_presence_of :location
  validates_presence_of :distance
  validates_format_of :email, :with => /(\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z|^$)/i, :on => :create
  validates_uniqueness_of :email

  before_create do
    self.key = UUIDTools::UUID.random_create.to_s
  end

end
