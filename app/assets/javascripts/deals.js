//= require map



$(document).ready(function () {

		var map = new google.maps.Map(document.getElementById('map-canvas'), { zoom: 14, mapTypeId: google.maps.MapTypeId.ROADMAP });
		var mm = new mapManager(map);
		mm.setMap(map);
		mm.initialize();
		mm.initLocationInput(function(){mm.navigateToChosenAddress();});			
});

