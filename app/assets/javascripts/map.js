//= require jquery
//= require jquery_ujs
//= require jquery_ui
//= require jquery_ui_autocomplete

﻿// todo replace , 'region': 'au' with value based on firstly IP then if not use domain default


// region: mapManager 

function mapManager() {
    this.map = null;
    this.LatLngList = new Array();
    this.geocoder = new google.maps.Geocoder();
}

mapManager.prototype.setMap = function (map) {
    this.map = map;
    this.infowindow = new google.maps.InfoWindow({
        content: '',
        maxWidth: 300
    })
    this.chosenLocationMarker = new google.maps.Marker({
        map: map,
        icon: '/images/home.jpg'
    });
}

mapManager.prototype.loadPoints = function () {
    var mapManager = this;
    $.ajax("poi")
        .done(function (data) {

            for (var i = 0; i < data.length; i++) {
                details = data[i];
                var marker = new google.maps.Marker({
                    map: mapManager.map,
                    title: details.title,
                    position: new google.maps.LatLng(details.lat, details.lon)
                });
                google.maps.event.addListener(marker, 'click',
                    function (mkr, dets) {
                        return function () {
                            var contentString = '<div id="content">' +
                                '<div id="siteNotice">' +
                                '</div>' +
                                '<div class="iw_title">' + dets.title + '</div>' +
                                '<div id="bodyContent">' + dets.detail +
                                '<br/><a href="' + dets.url + '">Click Here To Buy This Deal</a>'
                            '</div>' +
                            '</div>';
                            if (
                                mapManager.infowindow.visible === false ||
                                    typeof mapManager.infowindow.id === 'undefined' ||
                                    mapManager.infowindow.id != dets.id) {
                                mapManager.infowindow.id = dets.id;
                                mapManager.infowindow.content = contentString;
                                mapManager.infowindow.open(mapManager.map, mkr);
                            }
                            else {
                                mapManager.infowindow.close();
                                mapManager.infowindow.id = null;
                            }
                        }
                    }(marker, details)
                );
            }
        });
}

mapManager.prototype.goToLocation = function (lat, long) {
    var mapManager = this;
    $("#latitude").val(lat);
    $("#longitude").val(long);
    var location = new google.maps.LatLng(lat, long);
    mapManager.chosenLocationMarker.setPosition(location);
    mapManager.map.setCenter(location);
    this.loadPoints();
}

mapManager.prototype.initialize = function () {

    var mapManager = this;

    // todo get this location from ip or whatever
    lat = -33.8145564;
    long = 151.16989480000007;
    mapManager.goToLocation(lat, long);

    if (navigator.geolocation) {
        //browserSupportFlag = true;
        navigator.geolocation.getCurrentPosition(function (position) {
                mapManager.goToLocation(position.coords.latitude, position.coords.longitude);
            }, function (err) {
                console.warn('ERROR(' + err.code + '): ' + err.message);
            },
            {timeout: 10000});
    }
}

mapManager.prototype.navigateToChosenAddress = function () {
    var mapManager = this;
    var address = $('#lq').val();
    mapManager.geocoder.geocode({ 'address': address + ' , Australia' }, function (results, status) {
        if (results.length > 0) {
            $("#lq").autocomplete('close');
            mapManager.goToLocation(results[0].geometry.location.lat(), results[0].geometry.location.lng());
        }
        else {
            alert('Sorry address or location \'' + address + '\' is not known by us. Please try another address.');
        }
    });
}

mapManager.prototype.reverseGeocode = function (lat, lng, f) {
    var mapManager = this;
    var latlng = new google.maps.LatLng(lat, lng);
    mapManager.geocoder.geocode({ 'latLng': latlng }, function (results, status) {
        if (status !== google.maps.GeocoderStatus.OK) {
            // Do nothing
        }
        if (status == google.maps.GeocoderStatus.OK) {
            f(results);
        }
    });
}
var currentwatch;

mapManager.prototype.initGeolocator = function () {
    var mapManager = this;
    var lat, lng;

    if (navigator.geolocation) {
        $('#usemylocation').click(function () {
            if (lat) {
                $('#lq').val(lat + ' ' + lng);
                mapManager.reverseGeocode(
                    lat,
                    lng,
                    function (results) {
                        if (results.length > 0) {
                            $('#lq').val(results[0].formatted_address);
                        }
                    }
                );
            }
        });

        currentwatch = navigator.geolocation.watchPosition(
            function (position) {
                $('#usemylocation_container').removeClass('vanish')
                lat = position.coords.latitude
                lng = position.coords.longitude
            },
            function (err) {
                console.warn('ERROR(' + err.code + '): ' + err.message);
            },
            {
                enableHighAccuracy: true,
                maximumAge: 60000,
                timeout: 5000
            }
        );
    }
}

mapManager.prototype.initLocationInput = function (selectAction) {
    var mapManager = this;
    cancelAutoComplete = false;
    $(function () {
        var autocomplete = $("#lq").autocomplete({
            source: function (request, response) {
                mapManager.geocoder.geocode({ 'address': request.term + ' , Australia' }, function (results, status) {
                    if (cancelAutoComplete) {
                        return null;
                    }
                    else {
                        response($.map(results, function (item) {
                            return {
                                label: item.formatted_address,
                                value: item.formatted_address,
                                latitude: item.geometry.location.lat(),
                                longitude: item.geometry.location.lng()
                            }
                        }));
                    }
                })
            },
        })

        autocomplete.keypress(function (e) {
            if (e.which == 13) {
                cancelAutoComplete = true;
                selectAction();
                return false;
            }
            else {
                cancelAutoComplete = false;
            }
        })

        $("#lq").on("autocompleteselect", function (event, ui) {
            mapManager.goToLocation(ui.item.latitude, ui.item.longitude);
        });

    })
    $('#locationbutton').click(selectAction);
}

// endregion: mapManager 


