//= require jquery

$(function() {
    var getmoreurl = $("#linknext").attr('href')
    $("#linknext").hide();
    addgetmorebutton();

function addgetmorebutton() {
    $("#deallist").append('<li style="text-align:center;"><input class="btn btn-primary btn-lg" id="getmoredetails" type="submit" value="Get More Deals"></li>')
    $("#getmoredetails").click(function()
    {
        $("#getmoredetails").parent().remove()
        $.ajax( getmoreurl )
            .done(function(data) {
                html = $(data)
                $("#deallist").append($('#deallist .deal',html))
                getmoreurl = $("#linknext", html).attr('href')

                if (getmoreurl) {
                    addgetmorebutton();
                }
                else
                {
                    $("#deallist").append('<li style="text-align:center;"><p>No more details found, search again.</p></li>')
                }

            })
            .fail(function() {
                alert( "Error getting more results" );
            })
    });

}

});