class SubscribeController < ApplicationController

  respond_to :html

  def index

  end

  def squeeze
    @subscriber = Subscriber.new
    @subscriber.distance = 10
    @subscriber.location = params[:lq]
    @location_text = params[:location_text]
    render :layout => 'squeeze'
  end

  def subscribe_options
    @subscriber = Subscriber.find_all_by_key(params['key']).first()
    redirect_to :controller => 'home', :action => 'index' if !@subscriber
  end

  def subscribe_options_post
    @subscriber = Subscriber.find_all_by_key(params[:subscriber][:key]).first()
    if @subscriber
      if params[:commit] == 'Unsubscribe'
        @subscriber.delete
        flash[:notice] = 'You are now unsubscribed'
        redirect_to :controller => 'home', :action => 'index'
      else
        @subscriber.location = params[:subscriber][:location]
        @subscriber.distance = params[:subscriber][:distance]
        @subscriber.save!
        flash[:notice] = 'Your options are now updated'
        render :action => 'subscribe_options'
      end
    else
      redirect_to :controller => 'home', :action => 'index'
    end
  end

  def subscribe
    @subscriber = Subscriber.new
    @subscriber.distance = 5
    @subscriber.location = params[:lq]
  end

  def subscribe_post
    @subscriber = Subscriber.new(params[:subscriber])
    if @subscriber.save
      DealsUpdateMailer.welcome_email(@subscriber).deliver
      flash[:notice] = 'You are now subscribed, and we have sent your email'
      redirect_to :controller => 'home', :action => 'find_deals', :lq => @subscriber.location
    else
      render :action => 'subscribe'
    end
  end

end