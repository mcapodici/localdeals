class HomeController < ApplicationController

  respond_to :html

  def index
  end

  def find_deals
    Geocoder.configure(
        lookup: :google,
        timeout: 3,
        units: :km
    )

    @query = params[:lq]
    @page = (params[:page] || "1").to_i
    @page = 1 if @page < 1 or @page > 1000

    finder = DealFinder.new
    georesult = finder.geo_search(@query)

    if georesult
      @not_recognised = false
      @lat = georesult.latitude
      @lon = georesult.longitude
      @points = finder.find_deals(@lat, @lon, @page).to_a
      @more_results = finder.results_per_page == @points.count
      @address = georesult.address
    else
      @not_recognised = true
      @points = []
    end
    respond_to do |format|
      format.html
    end
  end

  def go_to_deal
    url = DealFinder.new.get_url(params[:id])
    url ||= url_for({:only_path => false, :controller => 'home', :action => 'index'})
    redirect_to url, status: 302
  end
end

