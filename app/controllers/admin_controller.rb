class AdminController < ApplicationController

  before_filter :authenticate

  def index

  end

  def subscribers
    @subscribers = Subscriber.all(:order => 'created_at desc', :limit => 100)
    respond_to do |format|
      format.html
    end
  end

  def points
    mongo = MongoConnect.new()
    #@providers = mongo.points.distinct('provider')
    @providers = mongo.points.group({
        :key => :provider,
        :reduce => 'function(cur, result) { result.count += 1 }',
        :initial => { :count => 0 }
    } )
    respond_to do |format|
      format.html
    end
  end

  def scrapecache
    mongo = MongoConnect.new()
    mongo.scrapecache.ensure_index( { "key" => 1 } )
    @results = mongo.scrapecache.find({}, :fields => {:key=>1}).sort({:key=>1})
    respond_to do |format|
      format.html
    end
  end
  def scrapecache_contents
    mongo = MongoConnect.new()
    result = mongo.scrapecache.find({:key => params["key"]}).first()["data"]
    render json:result
  end
  def scrapecache_remove
    mongo = MongoConnect.new()
    mongo.scrapecache.remove({:key => params["key"]})
    redirect_to(:action=>"scrapecache")
  end

  protected

  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      username == 'admin' and ENV['ADMIN_PASSWORD'] and password == ENV['ADMIN_PASSWORD']
    end
  end
end
